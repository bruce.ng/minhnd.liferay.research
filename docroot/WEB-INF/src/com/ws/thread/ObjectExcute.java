/**
 * 
 */

package com.ws.thread;

import java.util.ArrayList;
import java.util.List;

import com.message.fac.Header;

/**
 * @author minhnd
 *
 */
public class ObjectExcute {
	
	private Header header = null;
	private String xmlData = null;
	private List<Object> objects = new ArrayList<Object>();
	
	public ObjectExcute(Header header, List<Object> lstObject, String xmlData) {
		this.header = header;
		this.objects = lstObject;
		this.xmlData = xmlData;
	}
	
	public Header getHeader() {
		return header;
	}
	
	public void setHeader(Header header) {
		this.header = header;
	}
	
	public String getXmlData() {
		return xmlData;
	}
	
	public void setXmlData(String xmlData) {
		this.xmlData = xmlData;
	}
	
	public List<Object> getObjects() {
		return objects;
	}
	
	public void setObjects(List<Object> objects) {
		this.objects = objects;
	}
	
}
