package com.ws.thread;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.message.BusinessUtils;

/**
 * 
 */
public class ProcessExecuteMessage implements Runnable {

	private static Log log = LogFactoryUtil.getLog(ProcessExecuteMessage.class);
	private Thread t;
	private int timeSleep = 1000;
	private String threadName;
	private BusinessUtils businessUtils = new BusinessUtils();

	public ProcessExecuteMessage(String name, int timeSleep) {
		threadName = name;
		this.timeSleep = timeSleep;
	}

	@Override
	public void run() {
		while(true) {
			try {
				ObjectExcute objectExcute = ThreadPassingMessage.lstData.poll();
				if (objectExcute != null) {
					log.debug("==Executing thread==" + t.getName() + "==Data==" + objectExcute.getObjects().size());
					
					// Neu la cac cang vu tich hop.
					businessUtils.insertTempTable(objectExcute.getHeader(), objectExcute.getObjects());
	
					// Add 20-10-2014
					BusinessInsertTableResultUtils.insertResultNotification(objectExcute.getHeader(), objectExcute.getObjects());
					if (checkCangVuTichHop(objectExcute)) {
						log.debug("===========Executing thread===================ThreadPassingMessageTichHopCangVu.init().add=====.");
						ThreadPassingMessageTichHopCangVu.init().add(objectExcute);
					}
				}
				Thread.sleep(timeSleep);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void start() {
		if (t == null) {
			t = new Thread(this, threadName);
			t.start();
			log.debug("===Starting-------------- " + t.getName());
		}
	}

	private boolean checkCangVuTichHop(ObjectExcute objectExcute) {
		
		return false;
	}
}
