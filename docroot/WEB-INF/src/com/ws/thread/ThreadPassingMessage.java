/**
 * 
 */

package com.ws.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.util.portlet.PortletProps;
import com.utils.NumberUtils;

/**
 *
 */
public class ThreadPassingMessage {
	
	private Log _log = LogFactoryUtil.getLog(ThreadPassingMessage.class);
	private static ThreadPassingMessage threadPassingMessage = null;
	
	public static ConcurrentLinkedQueue<ObjectExcute> lstData = new ConcurrentLinkedQueue<ObjectExcute>();
	
	private int numberProcess = 5;
	private int timeSleep = 1000;
	private static List<ProcessExecuteMessage> lstThread = new ArrayList<ProcessExecuteMessage>();
	
	public static List<String> lstConfig = new ArrayList<String>();	
	
	public static ThreadPassingMessage init() {
		try {
			if (threadPassingMessage == null) {
				
				int numberProcess = NumberUtils.convertToInt(PortletProps.get("minhnd.number.process"));
				int timeSleep = NumberUtils.convertToInt(PortletProps.get("minhnd.time.sleep.process"));
				
				lstConfig.add("192.168.1.111");
				
				threadPassingMessage = new ThreadPassingMessage(numberProcess, timeSleep);
				threadPassingMessage.startProcess();
			}
			return threadPassingMessage;
		} catch (Exception es) {
			es.printStackTrace();
		}
		return null;
	}
	
	public void add(ObjectExcute excute) {
		lstData.add(excute);
	}
	
	private ThreadPassingMessage(int numberProcess, int timeSleep) {
		this.numberProcess = numberProcess;
		this.timeSleep = timeSleep;
		_log.debug("==Initing ThreadPassingMessage with numberProcess==" + this.numberProcess + "==timeSleep===" + this.timeSleep);
	}
	
	private void startProcess() {
		ProcessExecuteMessage processExecuteMessage = null;
		
		if (lstThread != null && lstThread.size() == 0) {
			for (int i = 1; i <= this.numberProcess; i++) {
				processExecuteMessage = new ProcessExecuteMessage("Process " + i, this.timeSleep);
				processExecuteMessage.start();
				lstThread.add(processExecuteMessage);
			}
		}
	}
}
