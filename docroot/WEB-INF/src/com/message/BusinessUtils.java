package com.message;

import java.util.ArrayList;
import java.util.List;

import com.message.fac.Header;
import com.ws.thread.ObjectExcute;
import com.ws.thread.ThreadPassingMessage;

public class BusinessUtils {
	
	
	
	//Ham phan tich du lieu nhan tu NSW.
	public String receiveMessage(String dataInput) {
		Header header = null;
		String data = MessageUtils.replaceXML(dataInput.trim());
		
		header = ReadMessages.readXMLMessagesHeader(data);
		
		if (header != null) {
			//gui qua ben Cang Vu
			List<Object> objects = new ArrayList<Object>();
			objects.add(header);
			ThreadPassingMessage.init().add(new ObjectExcute(header, objects, dataInput));
		}
		
		return dataInput;
	}

	/** Neu la cac cang vu tich hop.*/
	public void insertTempTable(Header header, List<Object> objects) {
		
	}
}
