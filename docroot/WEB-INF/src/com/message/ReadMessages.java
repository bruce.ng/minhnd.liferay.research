package com.message;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.message.fac.Header;
import com.message.fac.HeaderFactory;

public class ReadMessages {
	
	private static Log log = LogFactoryUtil.getLog(ReadMessages.class);
	
	public static Header readXMLMessagesHeader(String xmlString) {
		try {
			log.info("ReadXMLMessagesHeader  ");
			XPath xPath = XPathFactory.newInstance().newXPath();
			String expression = "Envelope/Header";//"//Body/Content";//
			
			ByteArrayInputStream stream = new ByteArrayInputStream(xmlString.getBytes("UTF-8"));
			Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(stream);
			Node node = (Node) xPath.compile(expression).evaluate(doc, XPathConstants.NODE);
			DOMSource source = new DOMSource(node);
			
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty("indent", "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			
			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			
			transformer.transform(source, result);
			
			// create a JAXBContext capable of handling classes generated into
			// Document doc1 = XmlUtils.parseXmlFile("C://header.xml", false);
			
			JAXBContext jc = JAXBContext.newInstance(HeaderFactory.class);
			
			// create an Unmarshaller
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			String xmlHeader = sw.toString();
			
			//xmlString=xmlString.replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
			
			ByteArrayInputStream input = new ByteArrayInputStream(xmlHeader.getBytes("UTF-8"));
			
			// unmarshal a po instance document into a tree of Java content
			// objects composed of classes from the primer.po package.
			
			Header header = (Header) unmarshaller.unmarshal(input);
			return header;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
