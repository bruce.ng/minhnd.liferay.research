package com.message;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;


public class MessageUtils {
	
	private static Log log = LogFactoryUtil.getLog(BusinessUtils.class);
	
	/**
	 * Remove <?xml version="1.0" encoding="utf-8"?>
	 * */
	public static String replaceXML(String data) {
		try {
			String dataPrefix = data.substring(0, data.indexOf("?>") + 2).toLowerCase();
			if (dataPrefix.contains("version") && dataPrefix.contains("encoding")) {
				return data.substring(data.indexOf("?>") + 2, data.length());
			}
			return data;
		} catch (Exception e) {
			log.error(e);
		}
		return data;
	}
}
