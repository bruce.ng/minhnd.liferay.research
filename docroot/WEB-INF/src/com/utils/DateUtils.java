
package com.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
	
	public static String dateToString(String format, Date date) { // lay ra ngay, thang, nam, gio, phu, giay hien tai
		String stringResult = "";
		SimpleDateFormat sdf = null;
		try {
			sdf = new SimpleDateFormat(format);
			stringResult = sdf.format(date);
		} catch (Exception e) {
			stringResult = "";
		}
		return stringResult;
	}
	
	public static Date stringToDate(String sDate, String format) {
		Date result = null;
		SimpleDateFormat sdf = null;
		try {
			sdf = new SimpleDateFormat(format);
			result = sdf.parse(sDate);
		} catch (Exception e) {
		}
		return result;
	}
	
	public static boolean isDate(String sDate, String format) {
		SimpleDateFormat sdf = null;
		try {
			sdf = new SimpleDateFormat(format);
			sdf.parse(sDate);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
