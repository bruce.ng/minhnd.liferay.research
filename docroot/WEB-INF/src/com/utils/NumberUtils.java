/**
 * 
 */

package com.utils;

import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 */
public class NumberUtils {
	
	public static String parseLongToString(long value) {
		try {
			return String.valueOf(value);
		} catch (Exception es) {
			return null;
		}
	}
	
	public static String parseIntToString(int value) {
		try {
			return String.valueOf(value);
		} catch (Exception es) {
			return null;
		}
	}
	
	public static XMLGregorianCalendar convertFromDateToXMLGregorianCalendar(Date date) {
		XMLGregorianCalendar dateReturn = null;
		try {
			GregorianCalendar c = new GregorianCalendar();
			c.setTime(date);
			dateReturn = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		} catch (Exception es) {
			//			es.printStackTrace();
			try {
				dateReturn = DatatypeFactory.newInstance().newXMLGregorianCalendar();
			} catch (Exception e) {
				//				e.printStackTrace();
			}
		}
		return dateReturn;
	}
	
	/**
	 * Convert from String to Long
	 * @param value
	 * @return
	 */
	public static long convertToLong(String value) {
		try {
			return Long.parseLong(value.trim());
		} catch (Exception es) {
		}
		return 0;
	}
	
	/**
	 * Convert from String to Double
	 * @param value
	 * @return
	 */
	public static double convertToDouble(String value) {
		try {
			return Double.parseDouble(value.trim());
		} catch (Exception es) {
		}
		return 0;
	}
	
	/**
	 * Convert from String to Long
	 * @param value
	 * @return
	 */
	public static int convertToInt(String value) {
		try {
			return Integer.parseInt(value.trim());
		} catch (Exception es) {
		}
		return 0;
	}
	
	public static String formatIntToString(int data) {
		try {
			if (data < 10) { return "0" + data; }
		} catch (Exception es) {
		}
		return String.valueOf(data);
	}
	
}
