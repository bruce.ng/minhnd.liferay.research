package com.portlet.liverusers;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.UserTracker;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class LiveUsersPortlet
 */
public class LiveUsersPortlet extends MVCPortlet {
	public void getLiveUsers(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			Class<?> liveUsers = PortalClassLoaderUtil.getClassLoader().loadClass("com.liferay.portal.liveusers.LiveUsers");
			System.out.println(liveUsers);
			Method getSessionUsers = liveUsers.getDeclaredMethod("getSessionUsers", long.class);
			Object map = getSessionUsers.invoke(null, themeDisplay.getCompanyId());
			Map<String, UserTracker> sessionUsers = null;
			sessionUsers = (ConcurrentHashMap<String, UserTracker>) map;
			System.out.println(sessionUsers);
			actionRequest.setAttribute("portalLiveUsers", sessionUsers);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
