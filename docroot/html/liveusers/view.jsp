<%@page import="com.liferay.portal.model.RoleConstants"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.liferay.portal.service.UserGroupRoleLocalServiceUtil"%>
<%@page import="com.liferay.portal.model.UserGroupRole"%>
<%@page import="java.util.List"%>
<%@page import="java.util.concurrent.ConcurrentHashMap"%>
<%@page import="com.liferay.portal.model.UserTracker"%>
<%@page import="java.util.Map"%>
<%@ include file="/html/init.jsp"%>
<portlet:actionURL var="getLiveUsers" windowState="normal" name="getLiveUsers"></portlet:actionURL>
<h2>Portal Live Users/Portal Logged in Users</h2>
<form action="<%=getLiveUsers%>" name="getLiveUsersForm"  method="POST">
<input type="submit" name="GetPortalLiveUsers" id="GetPortalLiveUsers" value="Get Portal Live Users"/>
</form>

<%
if (renderRequest.getAttribute("portalLiveUsers") != null) {
	Map<String, UserTracker> sessionUsers = null;
	sessionUsers=(ConcurrentHashMap<String, UserTracker>)renderRequest.getAttribute("portalLiveUsers");
%>
<table border="1">
	<tr>
		<th>Company Id</th>
		<th>Email Address</th>
		<th>FullName</th>
		<th>User Agent</th>
		<th>Remote Host</th>
		<th>Remote Address</th>
		<th>Session Id</th>
	</tr>
<%

	for (Map.Entry<String, UserTracker> entry : sessionUsers.entrySet()) {
		UserTracker liveUserTracker = entry.getValue();%>
		<tr>
			<td><%=liveUserTracker.getCompanyId()%></td>
			<td><%=liveUserTracker.getEmailAddress()%></td>
			<td><%=liveUserTracker.getFullName()%></td>
			<td><%=liveUserTracker.getUserAgent()%></td>
			<td><%=liveUserTracker.getRemoteHost()%></td>
			<td><%=liveUserTracker.getRemoteAddr()%></td>
			<td><%=liveUserTracker.getSessionId()%></td>
		</tr>
	<%}%>
</table>
<%}%>

<portlet:renderURL var="urlUserRoles">
	<portlet:param name="jspPage" value="/html/liveusers/userRoles.jsp"/>
</portlet:renderURL>
<a href="<%=urlUserRoles.toString()%>">Working with Liferay User Roles</a>