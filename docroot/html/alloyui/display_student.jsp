<%@page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<portlet:defineObjects />

<portlet:renderURL var="addStundentRenderURL" windowState="normal"></portlet:renderURL>

<h2>Display Student Details</h2>
<br />
<a href="<%=addStundentRenderURL.toString()%>">Add Student</a>
<br />
<% if (SessionMessages.contains(renderRequest.getPortletSession(), "student-form-success")) { %>
		<liferay-ui:success key="student-form-success" message="Student form Submitted successfully and following are the details." />
<% } %>

<% if (SessionErrors.contains(renderRequest.getPortletSession(), "student-form-error")) { %>
		<liferay-ui:error key="student-form-error" message="Student form Submitted have problem Please try again." />
<% } %>

<%
	Map<String, String> studentMap = (Map<String, String>) renderRequest.getAttribute("studentMapObject");
	if (studentMap != null) {
%>
	<b>First Name: </b><%=studentMap.get("firstName")%><br/>
	<b>Last Name:</b><%=studentMap.get("lastName")%><br/>
	<b>Exam Fee: </b><%=studentMap.get("examFee")%><br/>
	<b>Gender: </b><%=studentMap.get("sex")%><br/>
	<b>Acadamic Year: </b><%=studentMap.get("acadamicYear")%><br/>
	<b>Address: </b><%=studentMap.get("address")%><br/>
	<b>Subjects Obtained:</b><br/>
<%
	}
%>


<%
	List<String> subjectsList = (List<String>) renderRequest.getAttribute("subjectsList");
	if (subjectsList != null) {
		for (String subject : subjectsList) {
		%>
			<%=subject%><br />
		<%
		}
	}
%>