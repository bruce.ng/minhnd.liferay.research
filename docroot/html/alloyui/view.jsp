<%@page import="com.liferay.portal.model.User"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.meera.jsonwebservices.db.model.Employee"%>
<%@page import="com.meera.jsonwebservices.db.model.impl.EmployeeImpl"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.dao.orm.QueryUtil"%>
<%@page import="com.meera.jsonwebservices.db.service.EmployeeLocalServiceUtil"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>

<portlet:defineObjects />


<portlet:actionURL var="addStudentActionURL" windowState="normal" name="addStudent">
</portlet:actionURL>

<h1>Student Form</h1>

<%
	List<Employee> lst = EmployeeLocalServiceUtil.getEmployees(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	System.out.println(lst);
	System.out.println(Validator.isNull(lst) ? "null" : "not null");
	for (Employee e : lst) {
		System.out.println(e.getEmplyeeName());
	}
	if (lst != null && lst.size() > 0) {
		System.out.println("connection successfull");
	} else {
		System.out.println("false");
	}
	
	List<User> lstUser = UserLocalServiceUtil.getUsers(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
	for (User u : lstUser) {
		System.out.println(u.getFullName());
	}
%>

<form action="<%=addStudentActionURL%>" name="studentForm" method="POST">
	<b>First Name</b><br />
	<input type="text" name="<portlet:namespace/>firstName" id="<portlet:namespace/>firstName" /><br />
	
	<b>Last Name</b><br />
	<input type="text" name="<portlet:namespace/>lastName" id="<portlet:namespace/>lastName" /><br />
	
	<b>Exam Fee</b><br />
	<input type="text" name="<portlet:namespace/>examFee" id="<portlet:namespace/>examFee" /><br />
	
	<b>Gender</b><br /> <input type="radio" name="<portlet:namespace/>sex" value="male">Male<br>
	<input type="radio" name="<portlet:namespace/>sex" value="female">Female<br />
	
	<b>Subjects Obtained</b><br />
	<input type="checkbox" name="<portlet:namespace/>subjects" value="Maths">Maths<br>
	<input type="checkbox" name="<portlet:namespace/>subjects" value="Physics">Physics<br>
	<input type="checkbox" name="<portlet:namespace/>subjects" value="Chemistry">Chemistry<br>
	<input type="checkbox" name="<portlet:namespace/>subjects" value="Bio Technology">Biotechnology<br>
	<input type="checkbox" name="<portlet:namespace/>subjects" value="Computer Science">Computer Science<br>
	
	<b>Academic Year</b> <br>
	<select name="<portlet:namespace/>acadamicYear">
		<option value="I Year">I Year</option>
		<option value="II Year">II Year</option>
		<option value="III Year">III Year</option>
		<option value="IV Year">IV Year</option>
	</select><br>
	
	<b>Address</b><br />
	<textarea rows="4" cols="50" name="<portlet:namespace/>address"></textarea><br />
	
	<input type="submit" name="addStudent" id="addStudent" value="Add Student" />
</form>