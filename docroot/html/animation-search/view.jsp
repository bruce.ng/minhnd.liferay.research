<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
<%@ taglib uri="http://liferay.com/tld/security" prefix="liferay-security"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<portlet:defineObjects />
<liferay-theme:defineObjects />

<style>
	.searchAnimcationContainer {
		display: inline-block;
		position: relative;
		height: 34px;
		padding-left: 36px;
		background-color: #9575be;
		top: -6px;
		z-index: 1000;
		border: 0px solid red;
	}
	
	.searchAnimcationContainer .searchButton {
		position: absolute;
		top: 0;
		left: 0;
		display: block;
		width: 35px;
		height: 35px;
	}
	
	.searchAnimcationContainer .searchInputDiv {
		position: relative;
		display: block;
		overflow: hidden;
		width: 0;
		left: 1px;
		text-align: left;
	}
	
	.searchInputDiv input[type="text"] {
		border: 0;
		border-left: 1px solid #d5cfcf;
		width: 260px;
		height: 18px;
		line-height: 18px;
		font-size: 12px;
		outline: 0;
		background-color: transparent;
		background: 0;
		color: #fff;
		font-weight: bold;
	}
</style>
<portlet:actionURL var="actionURL"></portlet:actionURL>

<div class="searchAnimcationContainer" id="searchAnimcationContainer">
	<form action="<%=actionURL.toString()%>" method="post" name="themeSearchForm" id="themeSearchForm">
		<div class="searchButton" id="searchButton">
			<a href="#"><img src="<%=renderRequest.getContextPath()%>/images/btn-search.png" /></a>
		</div>
		<div class="searchInputDiv" id="searchInputDiv" style="width: 1px;">
			<input type="text" value="" placeholder="Enter keywords" name="keywords" id="keywords">
		</div>
	</form>
</div>

<aui:script>
	//anim
	AUI().use('aui-base', 'aui-node', 'aui-io', 'aui-event', 'anim',
		function(A) {
			// intiate animation over searchInputDiv  element
			var b = new A.Anim({
				node : '#searchInputDiv',
				duration : 0.1,
				easing : A.Easing.bounceOut
			});
			
			//on mouseover event so that when we movuse over on searchAnimcationContainer 
			//element then animation will apply
			
			A.one('#searchAnimcationContainer').on('mouseenter',
				function(event) {
					// define animation property array with width and opacity
					var toArrat = {
						width : 300,
						opacity : 0.5
					};
					b.set("to", toArrat);
					//run animation
					b.run();
			});
			
			//on mouseleave event so that when we mouseleave on searchAnimcationContainer element then 
			//animation will apply here we will bring back animation to inti	
			A.one('#searchAnimcationContainer').on('mouseleave',
				function(event) {
					var toArrat1 = {
						width : 1,
						opacity : 0.5
					};
					b.set("to", toArrat1);
					b.run();
					b.destroy();
			});
			A.one('#searchButton').on('click', function(event) {
				document.themeSearchForm.submit();
			});
		});
</aui:script>